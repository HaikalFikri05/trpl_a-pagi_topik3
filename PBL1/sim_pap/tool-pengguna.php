<!DOCTYPE html>
<html lang="en">
<?php
require("koneksi.php");
session_start();
$id        = $_SESSION['sesi_id'];
$username  = $_SESSION['sesi_user'];
$role      = $_SESSION['sesi_role'];
if (!isset($role)) {
  echo "<script>window.location='sign-in.php'</script>";
}
?>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="styleTool.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css2?family=Nerko+One&display=swap" rel="stylesheet">
  <script src="js/bootstrap.min.js"></script>
  <title>Tool</title>
</head>

<body>
  <!-- Header -->
  <section id="header">
    <div class="header container">
      <div class="nav-bar">
        <div class="brand">
          <a href="#hero">
            <img src="img/logo.JPG" style="width: 100px; height: 90px; border-radius: 20px">
          </a>
        </div>
        <div class="nav-list">
          <div class="hamburger">
            <div class="bar"></div>
          </div>
          <ul>
            <li><a href="home-pengguna.php" data-after="Home">Home</a></li>
            <li><a href="sign-out.php" data-after="Sign-out">Sign-out</a></li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <!-- End Header -->


  <!-- Hero Section  -->
  <section id="hero">
    <div class="hero container">
      <div>
        <h1>Welcome <span></span></h1>
        <h1>To <span></span></h1>
        <h1>SIM PAP <span></span></h1>
      </div>
    </div>
  </section>
  <!-- End Hero Section  -->

  <!-- Service Section -->
  <section id="services">
    <div class="services container">
      <div class="service-top">
        <h1 class="section-title">To<span>o</span>l</h1>
        <p>Alat praktikum untuk mahasiswa </p>
      </div>
      <div class="service-bottom">





        <?php
        include 'koneksi.php';
        $kode_barang = 1;
        $sql_barang = mysqli_query($koneksi, "SELECT * from barang");
        $sql_peminjaman = mysqli_query($koneksi, "SELECT * FROM peminjaman WHERE id_peminjaman = (SELECT MAX(id_peminjaman) FROM peminjaman)");
        while ($data_peminjaman = mysqli_fetch_array($sql_peminjaman)) {
          $id_peminjaman = $data_peminjaman['id_peminjaman'];
          $id_peminjaman++;
        }
        while ($data_barang = mysqli_fetch_array($sql_barang)) {
          $id_barang = $data_barang['kode_barang'];
        ?>
          <!-- <tr>
            <td></td>
            <td></td>
            <td><?php echo $data_barang['jumlah_barang']; ?></td>
            <td><?php echo "<img src='image/" . $data['foto'] . "' width='100'"; ?></td>
          </tr> -->

          <div class="service-item" style="align-items: center;">
            <div class="icon">
              <img src="<?php echo 'image/' . $data_barang['foto']; ?>" style="width: 150px; margin-left: -30px;" />
            </div>
            <h2><?php echo $data_barang['nama_barang']; ?></h2>
            <?php

            $query = mysqli_query($koneksi, "SELECT * FROM peminjaman WHERE kode_barang = $id_barang and id_pengguna = $id and status_pengembalian = 0 order by id_peminjaman desc limit 1");

            if ($data_barang['stok'] == 0){
            ?>  
            <button type="button" class="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#<?= "Data" . $kode_barang ?>" disabled>
                Tidak Tersedia
            </button>
            <?php } else if (mysqli_num_rows($query) == 0) {
            ?>
              <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#<?= "Data" . $kode_barang ?>">
                Pinjam
              </button>
            <?php } else {
              $last_penimjaman = mysqli_fetch_assoc($query)['id_peminjaman'];
            ?>
              <a href="pengembalian.php?id=<?= $last_penimjaman ?>" type="button" class="btn btn-danger">
                Kembalikan
              </a>
            <?php } ?>
          </div>

          <div class="modal fade" id="<?= "Data" . $kode_barang ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
                <div class="modal-header">
                  <?php //echo "Data" . $kode_barang
                  ?>

                  <h5 class="modal-title"><?php echo $data_barang['nama_barang']; ?></h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                  <div class="img-modal" style="display: flex;">
                    <img src="<?php echo 'image/' . $data_barang['foto']; ?>" alt="" style="width: 100px; height: 100px; margin: 60px 0 0 60px;">
                    <center>
                      <form action="simpan-tool.php" method="POST" class="row g-3 needs-validation" style="margin-top: 50px" enctype="multipart/form-data">
                        <div class="col-md-12 position-relative">
                          <table>
                            <tr>
                              <!-- <td>Id Peminjaman</td> -->
                              <td><input type="number" name="id_peminjaman" value="<?php echo $id_peminjaman; ?>" hidden></td>
                            </tr>
                            <tr>
                              <!-- <td>Id Pengguna</td> -->
                              <td><input type="text" name="id_pengguna" value="<?php echo $id; ?>" hidden></td>
                            </tr>
                            <tr>
                              <!-- <td>Kode Barang</td> -->
                              <td><input type="number" name="kode_barang" value="<?php echo $kode_barang ?>" hidden></td>
                            </tr>
                            <tr>
                              <td>Nama Barang</td>
                              <td><input type="text" name="nama_barang" value="<?php echo $data_barang['nama_barang']; ?>"></td>
                            </tr>
                            <tr>
                              <td>Jumlah Peminjaman</td>
                              <td><input type="number" name="jumlah_barang" max=1 required></td>
                            </tr>
                            <tr>
                              <td>Tanggal Peminjam</td>
                              <td><input type="date" name="tanggal_pinjam" value="" required></td>
                            </tr>
                            <tr>
                              <td>Tanggal Kembali</td>
                              <td><input type="date" name="tanggal_kembali" value="" required=""></td>
                            </tr>
                          </table>
                        </div>
                        <div class="invalid-tooltip">
                          Please select a valid state.
                        </div>
                        <div class="col-12">
                          <button class="btn btn-primary" type="submit" onclick="return confirm('Apakah Anda yakin memilih alat tersebut?')">Submit form</button>
                        </div>
                      </form>
                    </center>
                  </div>
                  <p>Jumlah Stock:
                    <?php echo $data_barang['stok']; ?>
                  </p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
        <?php
          $kode_barang++;
        }
        ?>

      </div>
    </div>
  </section>
  <!-- End Service Section -->


  <script src="js/app.js"></script>

</body>

</html>