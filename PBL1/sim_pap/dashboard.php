<!DOCTYPE html>
<html>
<?php

use LDAP\Result;

require("koneksi.php");
session_start();
$id        = $_SESSION['sesi_id'];
$username  = $_SESSION['sesi_user'];
$role      = $_SESSION['sesi_role'];
if (!isset($role)) {
    echo "<script>window.location='sign-in.php'</script>";
}
?>

<?php
$sel = "SELECT * FROM pengguna WHERE username = '$username'";
$query = mysqli_query($koneksi, $sel);
$resul = mysqli_fetch_assoc($query);
?>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="fontawesome/css/all.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="dashboard.css">
    <script src="js/jquery-3.6.1.js"></script>
    <title>Dasboard</title>
</head>

<body>

    <section id="menu">
        <div class="logo">
            <img src="img/logo.jpg" alt="sim pap">
            <h2>SIM PAP</h2>
        </div>

        <div class="items">
            <li><i class="fa-solid fa-house"></i><a href="home-pengguna.php">Home</a></li>
            <li><i class="fa-solid fa-chart-pie"></i><a href="dashboard.php">History</a></li>
            <li><i class="fa-solid fa-toolbox"></i><a href="dashboardTool.php">Tools</a></li>
            <li style="margin-top: 200px;"><a href="sign-out.php"><i class="fa-solid fa-right-from-bracket"></i>Log Out</a></li>
        </div>
    </section>


    <section id="interface">
        <div class="navigation">
            <div class="n1">
                <i id="menu-btn" class="fas fa-bars"></i>
            </div>
            <div class="profile">
                <i class="fa-solid fa-user"><span class="profile-admin" style="margin-left: 10px;"><span>
                            <?php if ($role == 'staff') {
                                echo $resul['nama_pengguna'];
                            } ?>
                        </span></i>
            </div>
        </div>

        <div class="i-name" style="display: flex; justify-content: space-between;">
            <h3>
                List History Peminjaman
            </h3>

            <a href="laporan.php"><button type="button" class="btn btn-primary">Print</button></a>
        </div>



        <div class="board">
            <table width="100%">
                <thead>
                    <tr>
                        <td>Nomor</td>
                        <td>ID Peminjaman</td>
                        <td>ID Pengguna</td>
                        <td>Nama Barang</td>
                        <td>Jumlah Barang</td>
                        <td>Tanggal Pinjam</td>
                        <td>Tanggal Kembali</td>
                        <td>Pengembalian</td>
                    </tr>
                </thead>
                <?php
                $tampil = mysqli_query($koneksi, "SELECT * from peminjaman");
                $no = 1;
                while ($hasil = mysqli_fetch_array($tampil)) {
                ?>
                    <tbody>
                        <tr>
                            <td><?php echo $no++; ?></td>
                            <td><?php echo $hasil['id_peminjaman']; ?></td>
                            <td><?php echo $hasil['id_pengguna']; ?></td>
                            <td><?php echo $hasil['nama_barang']; ?></td>
                            <td><?php echo $hasil['jumlah_barang']; ?></td>
                            <td><?php echo $hasil['tanggal_pinjam']; ?></td>
                            <td><?php echo $hasil['tanggal_kembali']; ?></td>
                            <td><?php echo $hasil['status_pengembalian'] ?></td>
                        </tr>
                    <?php
                }
                    ?>
                    </tbody>
            </table>
        </div>
    </section>

    <script src="main.js"></script>
    <script src="js/bootstrap.bundle.js"></script>

</body>

</html>