<!DOCTYPE html>
<html>
    <?php
      require("koneksi.php");
      session_start();
      $id        = $_SESSION['sesi_id'];
      $username  = $_SESSION['sesi_user'];
      $role      = $_SESSION['sesi_role'];
      if(!isset($role)){
      echo "<script>window.location='sign-in.php'</script>";
    }
  ?>

  <?php
    $sel = "SELECT * FROM pengguna WHERE username = '$username'";
    $query = mysqli_query($koneksi, $sel);
    $resul = mysqli_fetch_assoc($query);
  ?>
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="fontawesome/css/all.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="dashboardmhs.css">
  <script src="js/jquery-3.6.1.js"></script>
  <title>Dasboard</title>
</head>
<body>
  
    <section id="menu">
        <div class="logo">
            <img src="img/logo.jpg" alt="sim pap">
            <h2>SIM PAP</h2>
        </div>

        <div class="items">
            <li><i class="fa-solid fa-house"></i><a href="home-pengguna.php">Home</a></li>
            <li><i class="fa-solid fa-user"></i><a href="dashboardmhs.php">Profile</a></li>
            <li style="margin-top: 200px;"><a href="sign-out.php"><i class="fa-solid fa-right-from-bracket"></i>Log Out</a></li>
        </div>
    </section>


    <section id="interface">
        <div class="navigation">
            <div class="n1">
                <i id="menu-btn" class="fas fa-bars"></i>
            </div>
            <div class="profile">
                <i class="fa-solid fa-user"><span class="profile-admin" style="margin-left: 10px;"><span>
                    <?php IF($role == 'mhs'){
                        echo $resul['nama_pengguna'];
                    } ?>
                </span></i>
            </div>
        </div>

        <h3 class="i-name">
            Profile Mahasiswa
        </h3>


        <div class="board">
            <div class="table-profile">
                <table>
                    <tbody>
                        <tr>
                            <td>NIM</td>
                            <td>:&nbsp;</td>
                            <td>
                                <?php IF($role == 'mhs'){
                                echo $resul['NIM'];
                                } ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Name</td>
                            <td>:&nbsp;</td>
                            <td>
                                <?php IF($role == 'mhs'){
                                echo $resul['nama_pengguna'];
                                } ?>
                            </td>
                        </tr>
                        <tr></tr>
                        <tr>
                            <td>Kelas</td>
                            <td>:&nbsp;</td>
                            <td>
                                <?php IF($role == 'mhs'){
                                echo $resul['Kelas'];
                                } ?>
                            </td>
                        </tr> 
                        <tr>
                            <td>Prodi</td>
                            <td>:&nbsp;</td>
                            <td>
                                <?php IF($role == 'mhs'){
                                echo $resul['Prodi'];
                                } ?>
                            </td>
                        </tr>                  
                        <tr>
                            <td>Kontak</td>
                            <td>:&nbsp;</td>
                            <td>
                                <?php IF($role == 'mhs'){
                                echo $resul['kontak_pengguna'];
                                } ?>
                            </td>
                        </tr> 
                    </tbody>
                </table>
            </div>
        </div>
    </section>

<script src="main.js"></script>
<script src="js/bootstrap.bundle.js"></script>

</body>
</html>