<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="styleTool.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css2?family=Nerko+One&display=swap" rel="stylesheet">
  <script src="js/bootstrap.min.js"></script>
  <title>Tool</title>
</head>

<body>
  <!-- Header -->
  <section id="header">
    <div class="header container">
      <div class="nav-bar">
        <div class="brand">
          <a href="#hero">
            <img src="img/logo.JPG" style="width: 100px; height: 90px; border-radius: 20px">
          </a>
        </div>
        <div class="nav-list">
          <div class="hamburger">
            <div class="bar"></div>
          </div>
          <ul>
            <li><a href="home.html" data-after="Home">Home</a></li>
            <li><a href="sign-in.php" data-after="Sig-in">Sign-in</a></li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <!-- End Header -->


  <!-- Hero Section  -->
  <section id="hero">
    <div class="hero container">
      <div>
        <h1>Welcome <span></span></h1>
        <h1>To <span></span></h1>
        <h1>SIM PAP <span></span></h1>
      </div>
    </div>
  </section>
  <!-- End Hero Section  -->

  <!-- Service Section -->
  <section id="services">
    <div class="services container">
      <div class="service-top">
        <h1 class="section-title">To<span>o</span>l</h1>
        <p>Alat praktikum untuk mahasiswa </p>
      </div>
      <div class="service-bottom">





        <?php
        include 'koneksi.php';
        $kode_barang = 1;
        $sql = mysqli_query($koneksi, "SELECT * FROM barang");
        while ($data = mysqli_fetch_array($sql)) {
        ?>
          <!-- <tr>
            <td></td>
            <td></td>
            <td><?php echo $data['stok']; ?></td>
            <td><?php echo "<img src='image/" . $data['foto'] . "' width='100'"; ?></td>
          </tr> -->

          <div class="service-item" style="align-items: center;">
            <div class="icon">
              <img src="<?php echo 'image/' . $data['foto']; ?>" style="width: 150px; margin-left: -30px;" />
            </div>
            <h2><?php echo $data['nama_barang']; ?></h2>
            <p type="button" class="btn btn-secondary">Stock:
              <?php echo $data['stok']; ?>
            </p>
          </div>


          <div class="modal fade" id="<?= "Data" . $kode_barang ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title"><?php echo $data['nama_barang']; ?></h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                  <div class="img-modal" style="display: flex;">
                    <img src="<?php echo 'image/' . $data['foto']; ?>" alt="" style="width: 200px;">
                    <center>
                      <form class="row g-3 needs-validation" style="margin-top: 50px;">
                        <div class="col-md-12 position-relative">
                          <label for="validationTooltip04" class="form-label"></label>
                          <input type="number" name="jumlah" id="jumlah">
                        </div>
                        <div class="invalid-tooltip">
                          Please select a valid state.
                        </div>
                        <div class="col-12">
                          <button class="btn btn-primary" type="submit" onclick="return confirm('Apakah Anda yakin memilih alat tersebut?')">Submit form</button>
                        </div>
                      </form>
                    </center>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
        <?php
          $kode_barang++;
        }
        ?>

      </div>
    </div>
  </section>
  <!-- End Service Section -->
  <script src="js/app.js"></script>

</body>

</html>