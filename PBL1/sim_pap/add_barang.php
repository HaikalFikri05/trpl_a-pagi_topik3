<!DOCTYPE html>
<html>
<?php

use LDAP\Result;

require("koneksi.php");
session_start();
$id        = $_SESSION['sesi_id'];
$username  = $_SESSION['sesi_user'];
$role      = $_SESSION['sesi_role'];
if (!isset($role)) {
    echo "<script>window.location='sign-in.php'</script>";
}
?>

<?php
$sql_barang = mysqli_query($koneksi, "SELECT * from barang");
while($data_barang = mysqli_fetch_array($sql_barang)){
    $kode_barang = $data_barang['kode_barang'];
    $kode_barang++;
}
$sel = "SELECT * FROM pengguna WHERE username = '$username'";
$query = mysqli_query($koneksi, $sel);
$resul = mysqli_fetch_assoc($query);
?>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="fontawesome/css/all.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="dashboard.css">
    <script src="js/jquery-3.6.1.js"></script>
    <title>Dasboard</title>
</head>

<body>

    <section id="menu">
        <div class="logo">
            <img src="img/logo.jpg" alt="sim pap">
            <h2>SIM PAP</h2>
        </div>

        <div class="items">
            <li><i class="fa-solid fa-house"></i><a href="home-pengguna.php">Home</a></li>
            <li><i class="fa-solid fa-chart-pie"></i><a href="dashboard.php">History</a></li>
            <li><i class="fa-solid fa-toolbox"></i><a href="dashboardTool.php">Tools</a></li>
            <li style="margin-top: 200px;"><a href="sign-out.php"><i class="fa-solid fa-right-from-bracket"></i>Log Out</a></li>
        </div>
    </section>


    <section id="interface">
        <div class="navigation">
            <div class="n1">
                <i id="menu-btn" class="fas fa-bars"></i>
            </div>
            <div class="profile">
                <i class="fa-solid fa-user"><span class="profile-admin" style="margin-left: 10px;"><span>
                            <?php if ($role == 'staff') {
                                echo $resul['nama_pengguna'];
                            } ?>
                        </span></i>
            </div>
        </div>

        <h3 class="i-name">
            Tambah barang
        </h3>


        <div class="board">
            <table width="100%">
                <form action="upload.php" method="POST" enctype="multipart/form-data">
                    <table>
                        <tr>
                            <td>Kode Barang</td>
                            <td><input type="number" name="kode_barang" id="kode_barang" value="<?php echo $kode_barang ?>" readonly></td>
                        </tr>
                        <tr>
                            <td>Nama Barang</td>
                            <td><input type="text" name="nama" id="nama" required></td>
                        </tr>
                        <tr>
                            <td>Jumlah</td>
                            <td><input type="number" name="jumlah" id="jumlah" required></td>
                        </tr>
                        <tr>
                            <td>foto</td>
                            <td><input type="file" name="foto" id="foto" required></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="submit" name="simpan" value="Simpan"   ></td>
                        </tr>
                    </table>
                </form>
            </table>
        </div>
    </section>

    <script src="main.js"></script>
    <script src="js/bootstrap.bundle.js"></script>

</body>

</html>