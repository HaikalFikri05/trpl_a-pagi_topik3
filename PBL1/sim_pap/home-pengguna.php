<!DOCTYPE html>
<html>
<?php
    session_start();
      require("koneksi.php");
      $id        = $_SESSION['sesi_id'];
      $username  = $_SESSION['sesi_user'];
      $role      = $_SESSION['sesi_role'];
      if(!isset($role)){
      header("location:sign-in.php");

    }
  ?>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="js/bootstrap.min.js">
  <link href="https://fonts.googleapis.com/css2?family=Nerko+One&display=swap" rel="stylesheet">
  <title>home</title>
</head>

<body>
  <!-- Header -->
  <section id="header">
    <div class="header container">
      <div class="nav-bar">
        <div class="brand">
          <a href="#hero">
            <img src="img/logo.JPG" style="width: 100px; height: 90px; border-radius: 20px" alt="sim pap">
          </a>
        </div>
        <div class="nav-list">
          <div class="hamburger">
            <div class="bar"></div>
          </div>
          <ul>
          <?php IF($role == 'mhs'){ ?>
            <li><a href="#" data-after="Home">Home</a></li>
            <li><a href="#services" data-after="Service">Services</a></li>
            <li><a href="tool-pengguna.php" data-after="Tools">Tools</a></li>
            <li><a href="#contact" data-after="Contact">Contact</a></li>
            <li><a href="Dashboardmhs.php" data-after="Contact">Dashboard</a></li>
           <li><a href="sign-out.php" onclick="return confirm('Yakin Keluar?')" ><span class="glyphicon glyphicon-log-out"></span> Log Out</a></li>  
          </ul>
          <ul>
          <?php }ELSE IF($role == 'staff'){ ?>
            <li><a href="#" data-after="Home">Home</a></li>
            <li><a href="#services" data-after="Service">Services</a></li>
            <li><a href="#contact" data-after="Contact">Contact</a></li>
            <li><a href="dashboard.php" data-after="Tools">Dashboard</a></li>
           <li><a href="sign-out.php" onclick="return confirm('Yakin Keluar?')" ><span class="glyphicon glyphicon-log-out"></span> Log Out</a></li>  
          </ul>
        <?php } ?>
        </div>
      </div>
    </div>
  </section>
  <!-- End Header -->


  <!-- Hero Section  -->
  <section id="hero">
    <div class="hero container">
      <div>
        <h1>Welcome <span></span></h1>
        <h1>To <span></span></h1>
        <h1>SIM PAP <span></span></h1>
      </div>
    </div>
  </section>
  <!-- End Hero Section  -->

  <!-- Service Section -->
  <section id="services">
    <div class="services container">
      <div class="service-top">
        <h1 class="section-title">Serv<span>i</span>ces</h1>
        <p>SIM PAP merupakan aplikasi meminjam alat praktikum untuk mahasiswa </p>
      </div>
      <div class="service-bottom">
        <div class="service-item">
          <div class="icon"><img src="https://img.icons8.com/bubbles/100/000000/services.png"/></div>
          <h2>Informasi Web</h2>
          <p>Pusat informasi mahasiswa untuk meminjam alat praktikum jurusan teknik informatika.</p>
        </div>
        <div class="service-item">
          <div class="icon"><img src="https://img.icons8.com/bubbles/100/000000/services.png" /></div>
          <h2>Peminjaman</h2>
          <p>SIM PAP ini adalah pusat peminjaman alat praktikum untuk mahasiswa serta memudahkan mahasiswa mengakses untuk meminjam alat.</p>
        </div>
        <div class="service-item">
          <div class="icon"><img src="https://img.icons8.com/bubbles/100/000000/services.png" /></div>
          <h2>Informasi Alat</h2>
          <p>Alat yang bisa di pinjam seperti; Computer, Keyboard, CPU, Kabel lan, Mouse, Monitor, Camera, Gimbal, Proyektor/LCD, Planiameter, Distometer, Theodolite, Meteran</p>
        </div>
        <div class="service-item">
          <div class="icon"><img src="https://img.icons8.com/bubbles/100/000000/services.png" /></div>
          <h2>Jadwal</h2>
          <p>Berikut ini jadwal untuk peminjaman alat praktikum</p>
        </div>
      </div>
    </div>
  </section>
  <!-- End Service Section -->


  <!-- Contact Section -->
  <section id="contact">
    <div class="contact container">
      <div>
        <h1 class="section-title">Contact <span>info</span></h1>
      </div>
      <div class="contact-items">
        <div class="contact-item">
          <div class="icon"><img src="https://img.icons8.com/bubbles/100/000000/phone.png" /></div>
          <div class="contact-info">
            <h1>Phone</h1>
            <h2>0778 469860</h2>
          </div>
        </div>
        <div class="contact-item">
          <div class="icon"><img src="https://img.icons8.com/bubbles/100/000000/new-post.png" /></div>
          <div class="contact-info">
            <h1>Email</h1>
            <h2><a href="mailto:info@polibatam.ac.id"> Info@polibatam.ac.id</a></h2>
          </div>
        </div>
        <div class="contact-item">
          <div class="icon"><img src="https://img.icons8.com/bubbles/100/000000/map-marker.png" /></div>
          <div class="contact-info">
            <h1>Address</h1>
            <h2>Jl. Ahmad Yani, Batam, Kepulauan Riau 29461 Batam Kep. Riau</h2>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Contact Section -->

  <!-- Footer -->
  <section id="footer">
    <div class="footer container">
      <div class="brand">
        <h1><span>SIM</span> PAP</h1>
      </div>
      <p>Copyright ©2022 SIM PAP</p>
    </div>
  </section>
  <!-- End Footer -->
  
  
  
  <script src="js/app.js"></script>
  
</body>

</html>