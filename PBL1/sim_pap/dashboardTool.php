<!DOCTYPE html>
<?php
require("koneksi.php");
session_start();
$id        = $_SESSION['sesi_id'];
$username  = $_SESSION['sesi_user'];
$role      = $_SESSION['sesi_role'];
if (!isset($role)) {
    echo "<script>window.location='sign-in.php'</script>";
}
?>

<?php
$sel = "SELECT * FROM pengguna WHERE username = '$username'";
$query = mysqli_query($koneksi, $sel);
$resul = mysqli_fetch_assoc($query);
?>

<?php
$sql = mysqli_query($koneksi, "SELECT * FROM barang");
?>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="fontawesome/css/all.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="dashboard.css">
    <script src="js/jquery-3.6.1.js"></script>
    <title>Tool</title>
</head>

<body>


    <section id="menu">
        <div class="logo">
            <img src="img/logo.jpg" alt="sim pap">
            <h2>SIM PAP</h2>
        </div>

        <div class="items">
            <li><i class="fa-solid fa-house"></i><a href="home-pengguna.php">Home</a></li>
            <li><i class="fa-solid fa-chart-pie"></i><a href="dashboard.php">History</a></li>
            <li><i class="fa-solid fa-toolbox"></i><a href="dashboardTool.php">Tools</a></li>
            <li style="margin-top: 200px;"><a href="sign-out.php"><i class="fa-solid fa-right-from-bracket"></i>Log Out</a></li>
        </div>
    </section>


    <section id="interface">
        <div class="navigation">
            <div class="n1">
                <i id="menu-btn" class="fas fa-bars"></i>
            </div>
            <div class="profile">
                <i class="fa-solid fa-user"><span class="profile-admin" style="margin-left: 10px;"><span>
                            <?php if ($role == 'staff') {
                                echo $resul['nama_pengguna'];
                            } ?>
                        </span></i>
            </div>
        </div>

        <div class="i-name" style="display: flex; justify-content: space-between;">
            <h3>Tool</h3>

            <a href="add_barang.php"><button type="button" class="btn btn-primary"><i class="fa fa-plus"></i>Add Barang</button></a>
        </div>


        <div class="board">
            <table width="100%" style="text-align: center; background-color: white;">
                <form action="update.php" method="POST">
                    <thead>
                        <tr>
                            <th>No</th>
                            <td>Kode Barang</td>
                            <th>Nama barang</th>
                            <th>Jumlah Barang</th>
                            <th>Foto</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        include 'koneksi.php';
                        $kode_barang = 1;
                        while ($data = mysqli_fetch_array($sql)) {
                        ?>
                            <tr>
                                <td><?= $kode_barang++ ?></td>
                                <td><?php echo $data['kode_barang']; ?></td>
                                <td><?php echo $data['nama_barang']; ?></td>
                                <td><?php echo $data['stok']; ?></td>
                                <td><?php echo "<img src='image/" . $data['foto'] . "' width='100'"; ?></td>
                                <td>
                                    <a style="text-decoration: none; color: black; margin-left: 5px;" href="edit_tool.php?code=<?php echo $data['kode_barang']; ?>"><i class="fas fa-edit bg-success p-2 text-white rounded"></i></a>
                                    <a style="text-decoration: none; color: black;" href="hapusTool.php?code=<?php echo $data['kode_barang']; ?>"><i style="margin-left: 10px;" class="fas fa-trash-alt bg-danger p-2 text-white rounded"></i></a>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </form>
            </table>

        </div>


    </section>


    <script src="main.js"></script>
    <script src="js/bootstrap.bundle.js"></script>
</body>

</html>