<?php 

    include 'fpdf.php';
    include 'koneksi.php';

    $pdf=new FPDF("L", "cm", "A4");
    $pdf->SetMargins(0.5,1,1);
    $pdf->AliasNbPages();
    $pdf->AddPage();
    $pdf->SetFont('Arial', 'B', 14);
    $pdf->Cell(25.5,0.7,"Laporan SIM PAP",0,10,'C');
    $pdf->Ln(); 
    $pdf->Ln();
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(1,0.8,'NO',1,0);
    $pdf->Cell(3,0.8,'Id Peminjaman',1,0);
    $pdf->Cell(3,0.8,'Id Pengguna',1,0);
    $pdf->Cell(2.5,0.8,'Kode Barang',1,0);
    $pdf->Cell(3.5,0.8,'Nama Barang',1,0);
    $pdf->Cell(3,0.8,'Jumlah Barang',1,0);
    $pdf->Cell(3.5,0.8,'Tanggal Pinjam',1,0);
    $pdf->Cell(3.5,0.8,'Tanggal Kembali',1,0);
    $pdf->Cell(4.5,0.8,'Pengembalian',1,1,'C',0);
    $pdf->SetFont('Arial','',10);
    $no = 1;
    $tampil=mysqli_query($koneksi, "SELECT * FROM peminjaman");
    while($hasil = mysqli_fetch_array($tampil)){
        $pdf->Cell(1,0.8,$no,1,0);
        $pdf->Cell(3,0.8,$hasil['id_peminjaman'],1,0);
        $pdf->Cell(3,0.8,$hasil['id_pengguna'],1,0);
        $pdf->Cell(2.5,0.8,$hasil['kode_barang'],1,0);
        $pdf->Cell(3.5,0.8,$hasil['nama_barang'],1,0);
        $pdf->Cell(3,0.8,$hasil['jumlah_barang'],1,0);
        $pdf->Cell(3.5,0.8,$hasil['tanggal_pinjam'],1,0);
        $pdf->Cell(3.5,0.8,$hasil['tanggal_kembali'],1,0);
        $pdf->Cell(4.5,0.8,$hasil['status_pengembalian'],1,1,'C',0);
        $no++;
    }
    $pdf->Output("laporan_siswa.pdf", "I");

?>