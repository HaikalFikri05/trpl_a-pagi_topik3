/* Responsive */
$('#menu-btn').click(function(){
    $('#menu').toggleClass("active");
});



/* Tools Decrease and Increase */
/* 1 */
const plus = document.querySelector(".plus"),
minus = document.querySelector(".minus")
num = document.querySelector(".num");
let a = 1;
plus.addEventListener("click", ()=> {
    a++;

    a = (a < 10) ? "0" + a : a;
    num.innerText = a;
    
});
minus.addEventListener("click", ()=> {
    if (a > 1) {
    a--;
    a = (a < 10) ? "0" + a : a;
    num.innerText = a
    }
});


/* 2 */
const plus1 = document.querySelector(".plus1"),
minus1 = document.querySelector(".minus1")
num1 = document.querySelector(".num1");
let a1 = 1;
plus1.addEventListener("click", ()=> {
    a1++;

    a1 = (a1 < 10) ? "0" + a1: a1;
    num1.innerText = a1;
    
});
minus1.addEventListener("click", ()=> {
    if (a1 > 1) {
    a1--;
    a1 = (a1 < 10) ? "0" + a1 : a1;
    num1.innerText = a1;
    }
});


/* 3 */
const plus2 = document.querySelector(".plus2"),
minus2 = document.querySelector(".minus2")
num2 = document.querySelector(".num2");
let a2 = 1;
plus2.addEventListener("click", ()=> {
    a2++;

    a2 = (a2 < 10) ? "0" + a2: a2;
    num2.innerText = a2;
    
});
minus2.addEventListener("click", ()=> {
    if (a2 > 1) {
    a2--;
    a2 = (a2 < 10) ? "0" + a2 : a2;
    num2.innerText = a2;
    }
});


/* 4 */
const plus3 = document.querySelector(".plus3"),
minus3 = document.querySelector(".minus3")
num3 = document.querySelector(".num3");
let a3 = 1;
plus3.addEventListener("click", ()=> {
    a3++;

    a3 = (a3 < 10) ? "0" + a3: a3;
    num3.innerText = a3;
    
});
minus3.addEventListener("click", ()=> {
    if (a3 > 1) {
    a3--;
    a3 = (a3 < 10) ? "0" + a3 : a3;
    num3.innerText = a3;
    }
});


/* 5 */
const plus4 = document.querySelector(".plus4"),
minus4 = document.querySelector(".minus4")
num4 = document.querySelector(".num4");
let a4 = 1;
plus4.addEventListener("click", ()=> {
    a4++;

    a4 = (a4 < 10) ? "0" + a4: a4;
    num4.innerText = a4;
    
});
minus4.addEventListener("click", ()=> {
    if (a4 > 1) {
    a4--;
    a4 = (a4 < 10) ? "0" + a4 : a4;
    num4.innerText = a4;
    }
});


/* 6 */
const plus5 = document.querySelector(".plus5"),
minus5 = document.querySelector(".minus5")
num5 = document.querySelector(".num5");
let a5 = 1;
plus5.addEventListener("click", ()=> {
    a5++;

    a5 = (a5 < 10) ? "0" + a5: a5;
    num5.innerText = a5;
    
});
minus5.addEventListener("click", ()=> {
    if (a5 > 1) {
    a5--;
    a5 = (a5 < 10) ? "0" + a5 : a5;
    num5.innerText = a5;
    }
});


/* 7 */
const plus6 = document.querySelector(".plus6"),
minus6 = document.querySelector(".minus6")
num6 = document.querySelector(".num6");
let a6 = 1;
plus6.addEventListener("click", ()=> {
    a6++;

    a6 = (a6 < 10) ? "0" + a6: a6;
    num6.innerText = a6;
    
});
minus6.addEventListener("click", ()=> {
    if (a6 > 1) {
    a6--;
    a6 = (a6 < 10) ? "0" + a6 : a6;
    num6.innerText = a6;
    }
});


/* 8 */
const plus7 = document.querySelector(".plus7"),
minus7 = document.querySelector(".minus7")
num7 = document.querySelector(".num7");
let a7 = 1;
plus7.addEventListener("click", ()=> {
    a7++;

    a7 = (a7 < 10) ? "0" + a7: a7;
    num7.innerText = a7;
    
});
minus7.addEventListener("click", ()=> {
    if (a7 > 1) {
    a7--;
    a7 = (a7 < 10) ? "0" + a7 : a7;
    num7.innerText = a7;
    }
});


/* 9 */
const plus8 = document.querySelector(".plus8"),
minus8 = document.querySelector(".minus8")
num8 = document.querySelector(".num8");
let a8 = 1;
plus8.addEventListener("click", ()=> {
    a8++;

    a8 = (a8 < 10) ? "0" + a8: a8;
    num8.innerText = a8;
    
});
minus8.addEventListener("click", ()=> {
    if (a8 > 1) {
    a8--;
    a8 = (a8 < 10) ? "0" + a8 : a8;
    num8.innerText = a8;
    }
});


/* 10 */
const plus9 = document.querySelector(".plus9"),
minus9 = document.querySelector(".minus9")
num9 = document.querySelector(".num9");
let a9 = 1;
plus9.addEventListener("click", ()=> {
    a9++;

    a9 = (a9 < 10) ? "0" + a9: a9;
    num9.innerText = a9;
    
});
minus9.addEventListener("click", ()=> {
    if (a9 > 1) {
    a9--;
    a9 = (a9 < 10) ? "0" + a9 : a9;
    num9.innerText = a9;
    }
});


/* 11 */
const plus10 = document.querySelector(".plus10"),
minus10 = document.querySelector(".minus10")
num10 = document.querySelector(".num10");
let a10 = 1;
plus10.addEventListener("click", ()=> {
    a10++;

    a10 = (a10 < 10) ? "0" + a10: a10;
    num10.innerText = a10;
    
});
minus10.addEventListener("click", ()=> {
    if (a10 > 1) {
    a10--;
    a10 = (a10 < 10) ? "0" + a10 : a10;
    num10.innerText = a10;
    }
});


/* 12 */
const plus11 = document.querySelector(".plus11"),
minus11 = document.querySelector(".minus11")
num11 = document.querySelector(".num11");
let a11 = 1;
plus11.addEventListener("click", ()=> {
    a11++;

    a11 = (a11 < 10) ? "0" + a11: a11;
    num11.innerText = a11;
    
});
minus11.addEventListener("click", ()=> {
    if (a11 > 1) {
    a11--;
    a11 = (a11 < 10) ? "0" + a11 : a11;
    num11.innerText = a11;
    }
});


/* Show Hide Password */
$("#passwordfield").on("keyup",function(){
    if($(this).val())
        $(".glyphicon-eye-open").show();
    else
        $(".glyphicon-eye-open").hide();
    });
$(".glyphicon-eye-open").mousedown(function(){
                $("#passwordfield").attr('type','text');
            }).mouseup(function(){
              $("#passwordfield").attr('type','password');
            }).mouseout(function(){
              $("#passwordfield").attr('type','password');
            });