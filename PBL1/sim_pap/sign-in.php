<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="sign-in.css">
    <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@300;400&
    display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="fontawesome/css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Nerko+One&display=swap" rel="stylesheet">

    <title>SIM PAP</title>
</head>
<body>
    
       <div class="login" action="" method="POST">
            <img src="img/logo.jpg" alt="sim pap" class="logo">
            <h1 class="text-center">Welcome To</h1>
            <h1 class="text-center" style="font-size: 23px">SIM PAP</h1>
            <form action="proses_login.php" method="POST" class="needs-validation">
                <div class="form-group was-validated">
                    <label class="form-label" for="email">Username</label>
                    <input class="form-control" type="text" name="username" id="email" required>
                    <div class="invalid-feedback">
                        please enter your username
                    </div>
                </div>
                <div class="form-group was-validated">
                    <label  class="form-label" for="password">Password</label>
                    
                    <input class="form-control" type="password" name="password" id="password" required>
                    <span>
                    <div class="input-group-append">
       
                    </span>
                
                    <div class="invalid-feedback">
                        please enter your password
                        </div> 
                    </div>
                </div>
                <div class="form-group form-check was-validated">
                   <div class="checkbox">
                        <input class="form-label" type="checkbox" id="check">
                        <label for="check" style="font-size: 16px;">Remember me</label>
                        <a href="#" style="font-size: 16px;" class="remember">Remember pass?</a>
                   </div>
                </div>

               <input class="btn btn-primary w-100" type="submit" name="submit" value="SIGN IN" onclick="return confirm('Apakah Anda yakin?')" >
            </form>
       </div>


</body>
</html>